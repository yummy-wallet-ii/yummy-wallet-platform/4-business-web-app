export const environment = {
  production: true,
  apiUrl: 'https://yummy-wallet-server.ysoft.gr/api/v1/',
  socketUrl: 'https://yummy-wallet-server.ysoft.gr',
};
