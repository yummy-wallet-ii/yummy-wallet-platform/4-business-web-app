import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss'],
})
export class ConfirmDialogComponent implements OnInit {
  message: any;
  title: any;
  icon: any;
  iconColor: any;

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.icon = data.icon;
    this.title = data.title;
    this.message = data.message;

    switch (data.type) {
      case 'delete':
        this.iconColor = 'red';
        break;
      case 'update':
        this.iconColor = 'blue';
        break;
    }
  }

  ngOnInit(): void {}

  confirmDialog(): void {
    this.dialogRef.close(true);
  }

  cancelDialog(): void {
    this.dialogRef.close(false);
  }
}
