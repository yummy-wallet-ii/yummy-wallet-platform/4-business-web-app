import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss'],
})
export class TransactionComponent implements OnInit {
  hasSuccessfulScan: boolean;
  customerId: number;
  price: number;
  storeId: number | null;
  qrCode: any;
  isReady: boolean;

  constructor(
    private dialogRef: MatDialogRef<TransactionComponent>,
    // private dialog: MatDialog,
    // private transactionService: TransactionsService,
    // public toastMessagesService: ToastMessagesService
  ) {
    this.hasSuccessfulScan = false;
    this.customerId = -1;
    this.price = 0;
    this.isReady = false;
    this.storeId = localStorage.getItem('selectedStore')
      ? // @ts-ignore
        +localStorage.getItem('selectedStore')
      : null;
  }

  ngOnInit(): void {}

  closeDialog(): void {
    this.dialogRef.close();
  }

  /**
   *
   * @param qrData: the actual data transferred from the scan
   */
  scanSuccessHandler(qrData: string): void {
    if (qrData.includes('yummyWalletUserId')) {
      const tempData = qrData.split('=');
      this.customerId = +tempData[1];
      this.hasSuccessfulScan = true;
    }
  }

  /**
   * Enables the dialog, if the qr is valid and the price is set.
   */
  isFormInvalid(): boolean {
    return !(this.hasSuccessfulScan && this.price !== 0);
  }

  /**
   * A dialog alerts the user if they want to continue with the transaction.
   * Based on their response, the transaction is either completed or canceled.
   */
  async completeTransaction(): Promise<void> {
    this.closeDialog();
    // console.log(this.customerId);
    // console.log(
    //   `will send transaction for user ${this.customerId} with price ${this.price}`
    // );
    //
    // const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
    //   data: {
    //     icon: 'pay',
    //     title: 'Ολοκλήρωση συναλλαγής',
    //     message: `Είστε σίγουροι ότι θέλετε να ολοκληρώσετε τη συναλλαγή αξίας ${this.price}€?`,
    //   },
    // });
    //
    // confirmDialog.afterClosed().subscribe(async (result: any) => {
    //   if (result) {
    //     try {
    //       await this.transactionService.createTransaction(
    //         this.storeId,
    //         this.customerId,
    //         this.price
    //       );
    //       this.toastMessagesService.toastMessages(
    //         'Η συναλλαγή ολοκληρώθηκε με επιτυχία'
    //       );
    //       this.closeDialog();
    //     } catch (e) {
    //       this.toastMessagesService.toastErrorMessage(e.message);
    //     }
    //   }
    // });
  }

  setReady(isReady: boolean): void {
    this.isReady = isReady;
    this.qrCode = `https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl={
        "client": "yummywallet",
          "price": ${this.price},
          "merchantId": ${this.storeId}
         }`;
  }
}
