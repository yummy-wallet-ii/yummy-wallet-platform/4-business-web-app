import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-payment-amount',
  templateUrl: './payment-amount.component.html',
  styleUrls: ['./payment-amount.component.scss']
})
export class PaymentAmountComponent implements OnInit {
  @Input() amountFormGroup: FormGroup | undefined;
  amount: number;
  @Output() updateAmount: EventEmitter<number> = new EventEmitter<number>();
  constructor() {
    this.amount = 0;
  }

  ngOnInit(): void {
  }

  amountChanged() {
    this.updateAmount.emit(+this.amount);
  }
}
