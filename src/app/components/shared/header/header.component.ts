import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() title?: string;
  @Input() buttonTitle?: string;
  @Input() buttonIcon?: string;
  @Output() action: EventEmitter<any> = new EventEmitter<any>();
  @Input() hasAction?: boolean;
  @Input() hasDeleteAction?: boolean;
  @Output() deleteAction: EventEmitter<any> = new EventEmitter<any>();
  constructor() {
  }

  ngOnInit(): void {
  }

  resolveAction(): void {
    this.action.emit();
  }

  resolveDeleteAction(): void {
    this.deleteAction.emit();
  }
}
