import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtremeTableComponent } from './extreme-table.component';

describe('ExtremeTableComponent', () => {
  let component: ExtremeTableComponent;
  let fixture: ComponentFixture<ExtremeTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExtremeTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtremeTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
