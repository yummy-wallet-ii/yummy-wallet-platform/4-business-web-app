import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {DxDataGridComponent} from "devextreme-angular";
import {TableColumn} from "../../../models/TableColumn";

@Component({
  selector: 'app-extreme-table',
  templateUrl: './extreme-table.component.html',
  styleUrls: ['./extreme-table.component.scss']
})
export class ExtremeTableComponent implements OnInit {
  @Input() columns: TableColumn[] = [];
  @Input() data: any[] = [];
  @Input() noDataText: string = '';
  @Input() hasActions: boolean = false;
  @ViewChild('table', {static: false}) dataGrid!: DxDataGridComponent;
  currentFilter: any;
  showFilterRow: boolean;
  showHeaderFilter: boolean;
  applyFilterTypes: any;
  columnsLoaded: boolean;
  windowWidth: number;
  isLoading: boolean;
  viewName: string;
  isViewSaving: boolean;
  isViewSubmitting: boolean;
  views: any;
  searchEditorOptions: any;
  internalHeight: number;
  constructor() {
    this.isLoading = false;
    this.showFilterRow = true;
    this.showHeaderFilter = true;
    this.applyFilterTypes = [{
      key: 'auto',
      name: 'Immediately',
    }, {
      key: 'onClick',
      name: 'On Button Click',
    }];

    this.currentFilter = this.applyFilterTypes[0].key;
    this.orderHeaderFilter = this.orderHeaderFilter.bind(this);
    this.windowWidth = window.innerWidth - 272;
    this.columnsLoaded = false;
    this.viewName = '';
    this.isViewSaving = false;
    this.isViewSubmitting = false;
    this.views = {
      isDisplaying: false,
      isLoading: true,
      isDeleting: false,
      isUpdating: false,
      records: []
    };

    this.searchEditorOptions = {
      format: 'dd-MM-yyyy' // Specify the desired date format for search field
    };

    this.internalHeight = window.innerHeight - 272;
  }

  ngOnInit(): void {
  }





  //#region Extreme Table Helpers
  onExporting(e: any) {
  }
  displayCaption(label: string|null) {
    if (!label) {
      return '';
    }
    if (label.length > 40) {
      label = label.slice(0, 40) + '...';
    }

    return label;
  }
  orderHeaderFilter(data: { dataSource: { postProcess: (results: any) => any; }; }) {
    data.dataSource.postProcess = (results) => {
      results.push({
        text: 'Weekends',
        value: 'weekends',
      });
      return results;
    };
  }
  renderNonDateColumns(): any[] {
    return this.columns.filter(column => column.type !== 'date');
  }

  renderDateColumns(): any[] {
    return this.columns.filter(column => column.type === 'date');
  }
  //#endregion
}
