/* tslint:disable:variable-name typedef */
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { LoadingService } from '../../../services/loading.service';
import { ToastMessagesService } from '../../../services/toast-messages.service';
// @ts-ignore
import { loadStripe } from '@stripe/stripe-js';
import { CommonService } from '../../../services/common.service';
import { StoreService } from '../../../services/store.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-payment-transaction',
  templateUrl: './payment-transaction.component.html',
  styleUrls: ['./payment-transaction.component.scss'],
})
export class PaymentTransactionComponent implements OnInit, OnChanges {
  stripe: any;
  elements: any;
  intentSecret = '';
  payment: any;
  isSuccess: boolean;
  @Input() amount: number | undefined;
  @Input() isAmountCompleted = false;
  @Output() updateSuccessPaymentInitialization: EventEmitter<boolean> =
    new EventEmitter<boolean>();
  @ViewChild('paymentElement', { static: true }) paymentElement:
    | ElementRef
    | undefined;
  @Input() storeId: number | undefined | null;

  constructor(
    public loadingService: LoadingService,
    private commonService: CommonService,
    private router: Router,
    private storeService: StoreService,
    public toastService: ToastMessagesService
  ) {
    this.isSuccess = false;
  }

  async ngOnInit(): Promise<void> {}

  async ngOnChanges(changes: SimpleChanges): Promise<void> {
    try {
      const { isAmountCompleted, amount } = changes;
      if (isAmountCompleted?.currentValue) {
        await this.initializePayment();
        this.isSuccess = true;
        this.updateSuccessPaymentInitialization.emit(true);
      }
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      console.log('done');
    }
  }

  async initializePayment() {
    const appearance = {
      theme: 'flat',
      variables: { colorPrimaryText: '#262626' },
    };
    this.stripe = await loadStripe(
      'pk_test_51M2vwlD7jeb0MaCLA7alGnVFjQ34QISGsJw8pYCqj531TXronI5CQRtqUtcxapTCkqhjWTVoWXXy1W136i6EUOR800lOd2ppdK',
      {
        betas: ['payment_element'],
      }
    );
    const body = {
      merchantId: 5,
      amount: this.amount,
    };
    const client_secret = await this.commonService.postRequest(
      'transactions/init-payment-stripe',
      body
    );
    this.intentSecret = client_secret.clientSecret;
    this.elements = this.stripe.elements({
      clientSecret: client_secret.clientSecret,
      appearance,
    });
    this.payment = this.elements.create('payment');
    if (this.paymentElement) {
      this.payment.mount(this.paymentElement.nativeElement);
    }
  }

  async pay() {
    try {
      this.loadingService.setLoading(true);

      const { paymentIntent, error } = await this.stripe.confirmCardPayment(
        this.intentSecret,
        {
          payment_method: 'pm_card_visa',
        }
      );

      console.log(paymentIntent);

      if (paymentIntent.status === 'succeeded') {
        const body = {
          amount: this.amount,
          transactionIntentId: paymentIntent.id,
        };
        await this.storeService.refill(this.storeId, body);

        this.toastService.toastMessages(
          'Η συναλλαγή ολοκληρώθηκε με επιτυχία',
          null
        );

        setTimeout(async () => {
          await this.router.navigate(['dashboard/success']);
        }, 2000);
      }
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
    // const paymentIntent = await this.stripe.paymentIntents.confirm(
    //   this.intentSecret,
    //   {payment_method: 'pm_card_visa'}
    // );

    // this.stripe.confirmPayment({
    //   elements: this.elements,
    //   confirmParams: {
    //     // Return URL where the customer should be redirected after the PaymentIntent is confirmed.
    //     return_url: 'http://localhost:4200/#/dashboard/success',
    //   },
    // })
    //   .then((result: any) => {
    //     if (result.error) {
    //       console.log(result.error);
    //       // Inform the customer that there was an error.
    //     }
    //   });
  }
}
