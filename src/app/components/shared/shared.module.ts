import { NgModule } from '@angular/core';
import {LoadingComponent} from './loading/loading.component';
import { HeaderComponent } from './header/header.component';
import { MatIconModule } from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { ExtremeTableComponent } from './extreme-table/extreme-table.component';
import {FlexModule} from "@angular/flex-layout";
import {DxDataGridModule} from "devextreme-angular";
import {MatTooltipModule} from "@angular/material/tooltip";
import { PaymentComponent } from './payment/payment.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import { PaymentAmountComponent } from './payment-amount/payment-amount.component';
import { PaymentTransactionComponent } from './payment-transaction/payment-transaction.component';
import {MatStepperModule} from "@angular/material/stepper";


@NgModule({
  declarations: [
    LoadingComponent,
    HeaderComponent,
    ExtremeTableComponent,
    PaymentComponent,
    PaymentAmountComponent,
    PaymentTransactionComponent
  ],
  imports: [
    MatIconModule,
    CommonModule,
    MatButtonModule,
    FlexModule,
    DxDataGridModule,
    MatTooltipModule,
    FormsModule,
    MatInputModule,
    MatStepperModule,
    ReactiveFormsModule
  ],
  exports: [LoadingComponent, HeaderComponent, ExtremeTableComponent, PaymentComponent]
})
export class SharedModule { }
