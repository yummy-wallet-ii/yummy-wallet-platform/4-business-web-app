import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  @ViewChild('card-element') cardElement: ElementRef | undefined;
  @Input() storeId: number | null | undefined;
  amountFormGroup: FormGroup;
  paymentFormGroup: FormGroup;
  amountCompleted: boolean;
  stripe: any;
  constructor(
    private _formBuilder: FormBuilder,
  ) {
    this.amountCompleted = false;
    this.amountFormGroup = this._formBuilder.group({
      amount: ['', Validators.required]
    })
    this.paymentFormGroup = this._formBuilder.group({
      isSuccess:['', Validators.required]
    })
  }

  ngOnInit(): void {
  }

  setAmountCompleted(status: boolean) {
    this.amountCompleted = status;
  }

  updateAmount($event: any) {
    this.amountFormGroup.patchValue({
      amount: $event,
    });
  }

  getAmount(): number {
    // @ts-ignore
    return +this.amountFormGroup.get('amount').value
  }

  updateSuccessPaymentInitialization($event: any) {
    this.paymentFormGroup.patchValue({
      isSuccess: $event,
    });
  }
}
