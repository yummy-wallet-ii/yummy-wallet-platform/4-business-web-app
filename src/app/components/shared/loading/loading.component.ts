import {Component, OnInit, Input, OnChanges, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit, OnChanges {
  @Input() loading = false;
  height: number;
  constructor() {
    this.height = window.innerHeight - 60;
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: any): void {
    this.loading = changes.loading.currentValue;
  }

}
