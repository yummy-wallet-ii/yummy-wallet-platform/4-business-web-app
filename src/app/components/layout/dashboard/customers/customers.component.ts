import { Component, Input, OnInit } from '@angular/core';
import { LoadingService } from '../../../../services/loading.service';
import { Router } from '@angular/router';
import { ToastMessagesService } from '../../../../services/toast-messages.service';
import { CustomerService } from '../../../../services/customer.service';
import { MatDialog } from '@angular/material/dialog';
import { PushContentDialogComponent } from './push-content-dialog/push-content-dialog.component';
import { NotificationsService } from '../../../../services/notifications.service';
import { TableColumn } from '../../../../models/TableColumn';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss'],
})
export class CustomersComponent implements OnInit {
  customers: any[];
  columns: TableColumn[] = [];
  displayedColumns = ['firstName', 'lastName', 'identifier', 'mobile'];
  tableHeight: number;
  hasActions: boolean;
  storeId: number | null | undefined;

  constructor(
    private loadingService: LoadingService,
    private notificationService: NotificationsService,
    public router: Router,
    public dialog: MatDialog,
    public toastService: ToastMessagesService,
    private customerService: CustomerService
  ) {
    this.customers = [];
    this.tableHeight = window.innerHeight - 180;
    this.hasActions = false;
    this.columns = [
      {
        label: 'Όνομα',
        key: 'firstName',
        type: 'text',
        displayOnTable: true,
      },
      {
        label: 'Επώνυμο',
        key: 'lastName',
        type: 'text',
        displayOnTable: true,
      },
      {
        label: 'Αναγνωριστικό',
        key: 'identifier',
        type: 'text',
        displayOnTable: true,
      },
      {
        label: 'Τηλέφωνο',
        key: 'mobile',
        type: 'text',
        displayOnTable: true,
      },
    ];
  }

  ngOnInit(): void {
    // @ts-ignore
    this.storeId = +localStorage.getItem('selectedStore');
    this.initializeComponent();
  }

  async initializeComponent(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      this.customers = await this.customerService.fetchAll();
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  async send(): Promise<void> {
    const dialog = this.dialog.open(PushContentDialogComponent, {
      width: '50%',
    });

    dialog.afterClosed().subscribe(async (data) => {
      if (data) {
        try {
          const tokens = this.customers
            .filter((customer) => customer.fcmToken)
            .map((customer: any) => {
              if (
                customer.fcmToken !== null &&
                customer.fcmToken !== '' &&
                customer.fcmToken !== undefined
              ) {
                return customer.fcmToken;
              }
            });
          await this.notificationService.sendNotification(
            tokens,
            data,
            this.storeId
          );
        } catch (e) {
          this.toastService.toastErrorMessage(e.message);
        }
        // Send Notifications
      }
    });
  }
}
