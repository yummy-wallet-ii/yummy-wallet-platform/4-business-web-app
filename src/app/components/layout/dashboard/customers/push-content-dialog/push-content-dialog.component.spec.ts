import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PushContentDialogComponent } from './push-content-dialog.component';

describe('PushContentDialogComponent', () => {
  let component: PushContentDialogComponent;
  let fixture: ComponentFixture<PushContentDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PushContentDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PushContentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
