import { Component, OnInit } from '@angular/core';
import { Notification } from '../../../../../models/Notification';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-push-content-dialog',
  templateUrl: './push-content-dialog.component.html',
  styleUrls: ['./push-content-dialog.component.scss']
})
export class PushContentDialogComponent implements OnInit {
  notification: Notification = {} as Notification;
  constructor(public dialogRef: MatDialogRef<PushContentDialogComponent>) { }

  ngOnInit(): void {
  }

  cancelDialog(): void {
    this.close();
  }

  confirmDialog(): void {
    this.close(this.notification);
  }

  close(data?: Notification): void {
    this.dialogRef.close(data);
  }
}
