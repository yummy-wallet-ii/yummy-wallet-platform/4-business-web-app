import { Component, OnInit } from '@angular/core';
import { Category } from '../../../../models/Category';
import { CategoriesService } from '../../../../services/categories.service';
import { LoadingService } from '../../../../services/loading.service';
import { ToastMessagesService } from '../../../../services/toast-messages.service';
import { MatDialog } from '@angular/material/dialog';
import { CreateCategoryDialogComponent } from './create-category-dialog/create-category-dialog.component';
import { ConfirmDialogComponent } from '../../../dialogs/confirm-dialog/confirm-dialog.component';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  categories: Category[] = [];
  displayedColumns = ['image', 'id', 'name', 'description', 'actions'];
  tableHeight: number;

  constructor(private categoriesService: CategoriesService,
              private loadingService: LoadingService,
              public dialog: MatDialog,
              public domSanitizer: DomSanitizer,
              private toastMessagesService: ToastMessagesService) {
    this.tableHeight = window.innerHeight - 180;
  }

  ngOnInit(): void {
    this.initializeComponent();
  }

  async initializeComponent(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      this.categories = await this.fetchCategories();
      await this.fetchImages();
      console.log(this.categoriesService);
    } catch (e) {
      console.log(e);
      this.toastMessagesService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  createCategory(): void {
    const dialog = this.dialog.open(CreateCategoryDialogComponent, {
      disableClose: true,
      width: '650px',
      data: {
        isEdit: false
      }
    });

    dialog.afterClosed().subscribe(async (isCompleted: any) => {
      if (isCompleted) {
        this.categories = await this.fetchCategories();
        await this.fetchImages();
      }
    });
  }

  async fetchImages(): Promise<void> {
    for (const category of this.categories) {
      const image = await this.categoriesService.fetchCategoryImage(category.id);
      if (image !== null && image !== '') {
        category.imageBlob = `data:image/png;base64, ${image}`;
      }
    }
  }

  async fetchCategories(): Promise<Category[]> {
    return await this.categoriesService.getCategories();
  }

  update(id: number): void {
    const dialog = this.dialog.open(CreateCategoryDialogComponent, {
      disableClose: true,
      width: '650px',
      data: {
        isEdit: true,
        category: this.categories.find((category => category.id === id))
      }
    });

    dialog.afterClosed().subscribe(async () => {
      this.categories = await this.fetchCategories();
      await this.fetchImages();
    });
  }


  delete(id: number): void {
    const dialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Διαγραφή κατηγορίας',
        message: 'Είστε σίγουροι ότι θέλετε να διαγράψετε την κατηγορία?',
        icon: 'delete'
      }
    });

    dialog.afterClosed().subscribe(async (response: any) => {
      if (response) {
        await this.executeDeletion(id);
      }
    });
  }

  async executeDeletion(categoryId: number): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      await this.categoriesService.deleteCategory(categoryId);
      this.categories = await this.fetchCategories();
      await this.fetchImages();
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }
}
