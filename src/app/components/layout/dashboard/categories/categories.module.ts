import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesComponent } from './categories.component';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { SharedModule } from '../../../shared/shared.module';
import { CreateCategoryDialogComponent } from './create-category-dialog/create-category-dialog.component';
import { FlexModule } from '@angular/flex-layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import {MatCheckboxModule} from "@angular/material/checkbox";


@NgModule({
  declarations: [
    CategoriesComponent,
    CreateCategoryDialogComponent
  ],
    imports: [
        CommonModule,
        CategoriesRoutingModule,
        MatTableModule,
        MatIconModule,
        MatButtonModule,
        SharedModule,
        FlexModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        MatCheckboxModule
    ]
})
export class CategoriesModule { }
