import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Category } from '../../../../../models/Category';
import { LoadingService } from '../../../../../services/loading.service';
import { CategoriesService } from '../../../../../services/categories.service';
import { ToastMessagesService } from '../../../../../services/toast-messages.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-create-category-dialog',
  templateUrl: './create-category-dialog.component.html',
  styleUrls: ['./create-category-dialog.component.scss']
})
export class CreateCategoryDialogComponent implements OnInit {
  category: Category;
  loading: boolean;
  storeId: number | null | undefined;
  isEdit: boolean;
  image: string;
  constructor(public dialogRef: MatDialogRef<CreateCategoryDialogComponent>,
              private categoriesService: CategoriesService,
              public domSanitizer: DomSanitizer,
              public toastService: ToastMessagesService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private loadingService: LoadingService) {
    this.loading = false;
    this.isEdit = data.isEdit;
    this.storeId = localStorage.getItem('selectedStore')
      ? // @ts-ignore
      +localStorage.getItem('selectedStore')
      : null;
    this.category = data.isEdit ? data.category : this.initializeCategory();
    this.image = this.category.imageBlob ? this.category.imageBlob : '';

  }

  ngOnInit(): void {
  }

  private initializeCategory(): Category {
    return {
      id: 0,
      name: '',
      description: '',
      storeId: this.storeId ? this.storeId : 0,
      image: ''
    };
  }

  close(complete?: boolean): void {
    this.dialogRef.close(complete);
  }

  onFileInput($event: any): void {
    this.category.imageBlob = $event.target.files[0];

    const reader = new FileReader();
    reader.onload = () => {
      this.image = reader.result as string;
    };
    reader.readAsDataURL($event.target.files[0]);
  }

  isFormInvalid(): boolean {
    return this.category.name === '' || this.category.description === '' || this.loading;
  }

  async updateCategory(): Promise<void> {
    try {
      this.loading = true;
      console.log(this.category);
      await this.categoriesService.updateCategory(this.category);
      this.toastService.toastMessages('Η κατηγορία ενημερώθηκε με επιτυχία');
      this.close(true);
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loading = false;
    }
  }

  async createCategory(): Promise<void> {
    try{
      this.loading = true;
      const formData = new FormData();
      // @ts-ignore
      formData.append('file', this.category.imageBlob, this.category.imageBlob.name);
      formData.append('name', this.category.name);
      formData.append('description', this.category.description);
      // @ts-ignore
      formData.append('storeId', this.category.storeId);
      // formData.append('product', JSON.stringify(this.product));
      // formData.append('product', JSON.stringify(this.product));
      await this.categoriesService.createCategory(formData, this.category.storeId as number);
      this.toastService.toastMessages('Η κατηγορία δημιουργήθηκε με επιτυχία');
      this.close(true);
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loading = false;
    }
  }

  async updateImage(): Promise<void> {
    try{
      this.loading = true;
      const formData = new FormData();
      // @ts-ignore
      formData.append('file', this.category.imageBlob, this.category.imageBlob.name);
      formData.append('id', this.category.id.toString());

      // @ts-ignore
      await this.categoriesService.updateImage(formData, +this.category.storeId);
      this.toastService.toastMessages('Η εικόνα ενημερώθηκε με επιτυχία');
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loading = false;
    }
  }
}
