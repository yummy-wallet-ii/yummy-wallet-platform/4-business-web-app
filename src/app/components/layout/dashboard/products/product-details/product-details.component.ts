import { Component, OnInit } from '@angular/core';
import { Product } from '../../../../../models/Product';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from '../../../../../services/loading.service';
import { ToastMessagesService } from '../../../../../services/toast-messages.service';
import { ProductsService } from '../../../../../services/products.service';
import { Category } from '../../../../../models/Category';
import { CategoriesService } from '../../../../../services/categories.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../../../../dialogs/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  product: Product | undefined;
  productId: number | undefined;
  categories: Category[] = [];
  image = '';
  storeId: number | null | undefined;
  isNew: boolean;
  currentFile: any;

  constructor(public route: ActivatedRoute,
              public domSanitizer: DomSanitizer,
              public dialog: MatDialog,
              public categoriesService: CategoriesService,
              public loadingService: LoadingService,
              public router: Router,
              public toastService: ToastMessagesService,
              private productsService: ProductsService) {
    this.isNew = true;
  }

  ngOnInit(): void {
    this.initializeComponent();
  }

  async initializeComponent(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      this.storeId = localStorage.getItem('selectedStore')
        ? // @ts-ignore
        +localStorage.getItem('selectedStore')
        : null;
      this.productId = await this.getProductId();
      if (this.productId !== -1) {
        this.isNew = false;
        this.product = await this.productsService.getProduct(this.productId);
      } else {
        this.product = this.initializeNewProduct();
      }
      this.categories = await this.fetchCategories();

      if (this.productId !== -1) {
        await this.fetchImage();
      }
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  initializeNewProduct(): Product {
    return {
      id: -1,
      title: '',
      hasDiscount: false,
      price: 0,
      alternativePrice: 0,
      categoryId: -1,
      storeId: this.storeId,
      description: ''
    };
  }

  async fetchCategories(): Promise<Category[]> {
    return await this.categoriesService.getCategories();
  }

  async getProductId(): Promise<number> {
    return new Promise((resolve) => {
      this.route.params.subscribe(params => {
        resolve(+params.id);
      });
    });
  }


  async onFileInput($event: any): Promise<void> {
    try {
      // this.loadingService.setLoading(true);
      this.currentFile = $event.target.files[0];
      this.image = await this.getImage(this.currentFile);
      // @ts-ignore
      this.product.imageBlob = this.image;
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      // this.loadingService.setLoading(false);
    }
  }

  async fetchImage(): Promise<void> {
    console.log(this.productId);
    // @ts-ignore
    const image = await this.productsService.fetchImage(this.productId);
    if (image !== null && image !== '') {
      // @ts-ignore
      this.product.imageBlob = `data:image/png;base64, ${image}`;
      // @ts-ignore
      this.image = this.product?.imageBlob;
    }
  }

  async getImage(file: any): Promise<any> {
    return new Promise((resolve) => {
      if (file) {
        const reader = new FileReader();
        reader.onload = (e: any) => {
          resolve(e.target.result);
        };
        reader.readAsDataURL(file);
      }
    });
  }

  async updateProduct(): Promise<void> {
    if (this.isFormValid()) {
      try {
        this.loadingService.setLoading(true);
        await this.productsService.updateProduct(this.product);
        this.toastService.toastMessages('Η ενημέρωση του προϊόντος ολοκληρώθηκε με επιτυχία');
      } catch (e) {
        this.toastService.toastErrorMessage(e.message);
      } finally {
        this.loadingService.setLoading(false);
      }
    }
  }

  isFormValid(): boolean {
    if (!this.product?.imageBlob) {
      this.toastService.toastErrorMessage('Θα πρέπει να μεταφορτώσετε μια εικόνα για το προϊόν σας.');
      return false;
    }

    if (this.product.title === '' ||
      this.product.price.toString() === '' ||
      this.product.categoryId === -1) {
      this.toastService.toastErrorMessage('Τα πεδία τίτλος, τιμή και κατηγορία είναι υποχρεωτικά');
      return false;
    }

    return true;
  }

  async createProduct(): Promise<void> {
    if (this.isFormValid()) {
      const formData = new FormData();
      formData.append('file', this.currentFile, this.currentFile.name);
      delete this.product?.image;
      // @ts-ignore
      formData.append('description', this.product.description.toString());
      // @ts-ignore
      formData.append('categoryId', this.product.categoryId.toString());
      // @ts-ignore
      formData.append('title', this.product?.title.toString());
      // @ts-ignore
      formData.append('price', this.product?.price.toString());
      // @ts-ignore
      formData.append('alternativePrice', this.product?.alternativePrice.toString());
      // formData.append('product', JSON.stringify(this.product));

      try {
        this.loadingService.setLoading(true);
        // @ts-ignore
        await this.productsService.createProduct(formData, this.storeId);
        this.toastService.toastMessages('Η δημιουργία του προϊόντος ολοκληρώθηκε με επιτυχία');
      } catch (e) {
        this.toastService.toastErrorMessage(e.message);
      } finally {
        this.loadingService.setLoading(false);
      }
    }
  }

  discountChange(): void {
    // @ts-ignore
    this.product.hasDiscount = !this.product.hasDiscount;
  }

  async updateImage(): Promise<void> {
    try {
      const formData = new FormData();
      formData.append('file', this.currentFile, this.currentFile.name);
      // @ts-ignore
      formData.append('id', this.product.id.toString());
      this.loadingService.setLoading(true);
      // @ts-ignore
      await this.productsService.updateProductImage(formData, this.storeId);
      this.toastService.toastMessages('Η ενημέρωση φωτογραφίας του προϊόντος ολοκληρώθηκε με επιτυχία');
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  deleteAction(): void {
    const dialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        message: 'Είστε σίγουροι ότι θέλετε να διαγράψετε το προϊόν?',
        title: 'Διαγραφή προϊόντος',
        icon: 'delete'
      }
    });
    dialog.afterClosed().subscribe(async (result) => {
      if (result) {
        try {
          this.loadingService.setLoading(true);
          await this.productsService.deleteProduct(this.productId);
          await this.router.navigate(['dashboard/products']);
        } catch (e) {
          this.toastService.toastErrorMessage(e.message);
        } finally {
          this.loadingService.setLoading(false);
        }
      }
    });
  }
}
