import { Component, OnInit } from '@angular/core';
import { Product } from '../../../../models/Product';
import { LoadingService } from '../../../../services/loading.service';
import { ToastMessagesService } from '../../../../services/toast-messages.service';
import { ProductsService } from '../../../../services/products.service';
import { Router } from '@angular/router';
import { CategoriesService } from '../../../../services/categories.service';
import { Category } from '../../../../models/Category';
import { CommonService } from '../../../../services/common.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  products: Product[] = [];
  categories: Category[] = [];
  groupedProducts: any = [];
  pageHeight: number;
  storeId: number | null | undefined;
  pageLoaded = false;
  selectedCategories: Category[] = [];

  constructor(private loadingService: LoadingService,
              public router: Router,
              private commonService: CommonService,
              public toastService: ToastMessagesService,
              private categoriesService: CategoriesService,
              private productsService: ProductsService) {
    this.pageHeight = window.innerHeight - 200;
    this.storeId = localStorage.getItem('selectedStore')
      ? // @ts-ignore
      +localStorage.getItem('selectedStore')
      : null;
  }

  async ngOnInit(): Promise<void> {
    await this.initializeComponent();
  }

  async initializeComponent(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      // const all = {
      //   id: 0,
      //   name: 'all',
      //   description: 'Όλες'
      // };
      this.categories = await this.categoriesService.getCategories();
      // this.categories.unshift(all);
      // this.selectedCategories.push(all);
      this.products = await this.fetchProducts();
      this.groupedProducts = this.commonService.group(this.products, 'categoryId', { visible: true });
      this.pageLoaded = true;
    } catch (e) {
      console.log(e);
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  async fetchProducts(): Promise<Product[]> {
    // @ts-ignore
    return await this.productsService.getProducts(this.storeId);
  }

  async createProduct(): Promise<void> {
    await this.router.navigate([`/dashboard/product-details/-1`]);
  }

  getCategoryName(categoryId: number): string {
    const category = this.categories.find(c => +c.id === +categoryId);
    if (category) {
      return category.description;
    } else {
      return '';
    }
  }

  checkVisibility(): void {
    if (this.selectedCategories.length === 0) {
      // this.groupedProducts.map((c: any) => this.setProductCategoriesVisibility(c.categoryId, false));
    }

    if (this.selectedCategories.some((c: Category) => +c.id === 0)) {
      this.selectedCategories = this.selectedCategories.filter((c: Category) => c.id === 0);
      // this.groupedProducts.map((c: any) => this.setProductCategoriesVisibility(c.categoryId, true));
    } else {
      const groupedProducts = [...this.groupedProducts];
      const product: any = this.commonService.deepClone(groupedProducts[0]);
      console.log(product);
      product.visible = {
        visible: true
      };
      // this.groupedProducts = groupedProducts;
      console.log(groupedProducts[0].params.visible);
      // this.groupedProducts.map((gp: any) => {
      //   if (this.selectedCategories.some((c: Category) => +gp.categoryId === +c.id)) {
      //     console.log(gp.categoryId);
      //     gp.params.visible = true;
      //   } else {
      //     gp.params.visible = false;
      //   }
      // });
    }

    // console.log('visible');
    // console.log(this.groupedProducts.filter((c: any) => c.params.visible));
  }

  setProductCategoriesVisibility(categoryId: number, state: boolean): void {
    const gp = this.groupedProducts.find((c: any) => +c.categoryId === +categoryId);
    if (gp) {
      console.log(`changed category id: ${categoryId} from ${gp.params.visible} to ${state}`);
      gp.params.visible = state;
      console.log(gp);
    }
    // const index = this.groupedProducts.findIndex((productCategory: any) => +productCategory.categoryId === +categoryId);
    // if (index !== -1) {
    //   console.log(`changed category id: ${categoryId} to ${state}`);
    //   this.groupedProducts[index].params.visible = state;
    //   console.log(this.groupedProducts[index]);
    // }
  }
}
