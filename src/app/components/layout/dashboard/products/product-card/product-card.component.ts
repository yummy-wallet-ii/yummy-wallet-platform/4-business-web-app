import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../../../../../models/Product';
import { Router } from '@angular/router';
import { ProductsService } from '../../../../../services/products.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {
  @Input() product: Product | undefined;
  image = '';
  constructor(public router: Router,
              public domSanitizer: DomSanitizer,
              private productService: ProductsService) {
  }

  ngOnInit(): void {
    this.fetchImage();
  }

  async navigateToDetails(): Promise<void> {
    await this.router.navigate([`/dashboard/product-details/${this.product?.id}`]);
  }

  async fetchImage(): Promise<void> {
      // @ts-ignore
    const image = await this.productService.fetchImage(this.product.id);
    if (image !== null && image !== '') {
      // @ts-ignore
      this.product.imageBlob = `data:image/png;base64, ${image}`;
      // @ts-ignore
      this.image = this.product?.imageBlob;
      }
    }
}
