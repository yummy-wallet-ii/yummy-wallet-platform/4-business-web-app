import {Component, OnInit} from '@angular/core';
import {TableColumn} from "../../../../models/TableColumn";
import {StoreService} from "../../../../services/store.service";
import {ToastMessagesService} from "../../../../services/toast-messages.service";
import {LoadingService} from "../../../../services/loading.service";

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  refillColumns: TableColumn[] = [];
  refillData: any[] = [];
  storeId: number | null;
  constructor(private storeService: StoreService,
              public toastService: ToastMessagesService,
              public loadingService: LoadingService) {
    // @ts-ignore
    this.storeId = +localStorage.getItem('selectedStore');

    this.refillColumns = [{
      label: 'Ποσό',
      key: 'amount',
      type: 'text',
      displayOnTable: true
    },
      {
        label: 'Ημερομηνία συναλλαγής',
        key: 'transactionDate',
        type: 'date',
        displayOnTable: true
      }];
  }

  async ngOnInit(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      await this.getRefills();
    } catch (e) {
      console.log(e);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  async getRefills() {
    const data = await this.storeService.fetchRefills(this.storeId);
    console.log(data);
    this.refillData = data;
  }
}
