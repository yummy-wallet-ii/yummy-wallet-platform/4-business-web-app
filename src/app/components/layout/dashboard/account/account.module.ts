import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { AccountComponent } from './account.component';
import {FlexModule} from "@angular/flex-layout";
import {SharedModule} from "../../../shared/shared.module";
import {DxTabsModule} from "devextreme-angular";
import {MatTabsModule} from "@angular/material/tabs";


@NgModule({
  declarations: [
    AccountComponent
  ],
    imports: [
        CommonModule,
        AccountRoutingModule,
        FlexModule,
        SharedModule,
        DxTabsModule,
        MatTabsModule
    ]
})
export class AccountModule { }
