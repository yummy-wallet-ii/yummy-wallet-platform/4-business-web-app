import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LogsRoutingModule } from './logs-routing.module';
import { LogsComponent } from './logs.component';
import { MatTableModule } from '@angular/material/table';
import { CustomDatePipe } from '../../../shared/pipes/custom.datepipe';
import { FlexModule } from '@angular/flex-layout';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  declarations: [
    LogsComponent,
    CustomDatePipe
  ],
  imports: [
    CommonModule,
    LogsRoutingModule,
    MatTableModule,
    FlexModule,
    SharedModule
  ]
})
export class LogsModule { }
