import { Component, OnInit } from '@angular/core';
import { Log } from '../../../../models/Log';
import { MatTableDataSource } from '@angular/material/table';
import { StoreService } from '../../../../services/store.service';
import { LoadingService } from '../../../../services/loading.service';
import { ToastMessagesService } from '../../../../services/toast-messages.service';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss']
})
export class LogsComponent implements OnInit {
  logs: MatTableDataSource<Log>;
  displayColumns = [ 'message', 'actionDescription', 'typeDescription', 'identifier', 'createdAt'];
  tableHeight: number;
  storeId: number | null;
  constructor(private storeService: StoreService,
              public toastService: ToastMessagesService,
              private loadingService: LoadingService,) {
    this.logs = new MatTableDataSource<Log>();
    this.storeId = localStorage.getItem('selectedStore')
      ? // @ts-ignore
      +localStorage.getItem('selectedStore')
      : null;
    this.tableHeight = window.innerHeight - 200;
  }

  async ngOnInit(): Promise<void> {
    this.loadingService.setLoading(false);
    await this.fetchLogs();
  }

  async fetchLogs(): Promise<void> {
    try {
      const logs: Log[] = await this.storeService.fetchStoreLogs(this.storeId);
      this.logs = new MatTableDataSource<Log>(logs);
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }
}
