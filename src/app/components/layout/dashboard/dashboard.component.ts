import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { MatDialog } from '@angular/material/dialog';
import { LogoutDialogComponent } from '../../dialogs/logout-dialog/logout-dialog.component';
import { Router } from '@angular/router';
import { SideBarItem } from '../../../models/SideBarItem';
import { UserService } from '../../../services/user.service';
import { ToastMessagesService } from '../../../services/toast-messages.service';
import { LoadingService } from '../../../services/loading.service';
import { Subscription } from 'rxjs';
import { TransactionComponent } from '../../dialogs/transaction/transaction.component';
import { StoreService } from '../../../services/store.service';
import { NotificationsService } from '../../../services/notifications.service';
import { OrdersService } from '../../../services/orders.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  isSidebarOpened: boolean;
  sideBarItems: SideBarItem[] = [];
  user: any = null;
  loading = true;
  subscription: Subscription;
  pendingOrdersSubscription: Subscription;
  numberOfPendingOrders: number;
  constructor(
    private router: Router,
    private userService: UserService,
    private storeService: StoreService,
    private toastMessagesService: ToastMessagesService,
    private loadingService: LoadingService,
    private notificationService: NotificationsService,
    private ordersService: OrdersService,
    private dialog: MatDialog
  ) {
    this.isSidebarOpened = false;
    this.numberOfPendingOrders = 0;
    this.notificationService.initSocketCommunication();
    this.pendingOrdersSubscription = this.ordersService.observe().subscribe((data: number) => {
      this.numberOfPendingOrders = data;
    });
    this.subscription = this.loadingService.observe().subscribe(
      (state) => {
        this.loading = state;
      },
      (error) => this.toastMessagesService.toastErrorMessage(error.message)
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.pendingOrdersSubscription.unsubscribe();
  }

  async ngOnInit(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      await this.initializeUser();
      await this.initializeUserStores();
      this.initializeSideNav();
      await this.ordersService.fetchPending();
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  initializeSideNav(): void {
    const profile: SideBarItem = {
      title: 'Προφίλ επιχείρησης',
      openDialog: false,
      path: '/dashboard/profile',
      icon: 'equalizer',
    };

    const transaction: SideBarItem = {
      title: 'Νέα συναλλαγή',
      openDialog: false,
      path: '',
      icon: 'credit_card',
    };

    const categories: SideBarItem = {
      title: 'Κατηγορίες',
      openDialog: false,
      path: 'dashboard/categories',
      icon: 'category',
    };

    const products: SideBarItem = {
      title: 'Προϊόντα',
      openDialog: false,
      path: 'dashboard/products',
      icon: 'shopping_cart',
    };

    // const appointments: SideBarItem = {
    //   title: 'Ραντεβού',
    //   openDialog: false,
    //   path: 'dashboard/appointments',
    //   icon: 'calendar_month',
    // };

    const customers: SideBarItem = {
      title: 'Οι πελάτες μου',
      openDialog: false,
      path: 'dashboard/customers',
      icon: 'people',
    };

    const transactions: SideBarItem = {
      title: 'Συναλλαγές',
      openDialog: false,
      path: 'dashboard/transactions',
      icon: 'payments',
    };

    const orders: SideBarItem = {
      title: 'Παραγγελίες',
      openDialog: false,
      path: 'dashboard/orders',
      icon: 'compare_arrows',
      hasBadge: true,
    };

    const settings: SideBarItem = {
      title: 'Ρυθμίσεις',
      openDialog: false,
      path: '/dashboard/settings',
      icon: 'settings',
    };

    const logs: SideBarItem = {
      title: 'Καταγραφές ενεργειών',
      openDialog: false,
      path: '/dashboard/logs',
      icon: 'summarize',
    };

    const account: SideBarItem = {
      title: 'Ο Λογαριασμός μου',
      openDialog: false,
      path: '/dashboard/account',
      icon: 'credit_score',
    };

    this.sideBarItems.push(
      transaction,
      profile,
      orders,
      categories,
      products,
      customers,
      transactions,
      settings, logs,
      account
    );
  }

  async initializeUser(): Promise<void> {
    this.user = await this.userService.getPersonalInfo();
    console.log(this.user);
  }

  async initializeUserStores(): Promise<void> {
    const stores = await this.storeService.getUserStores();
    if (stores) {
      this.user.stores = stores;
      localStorage.setItem('selectedStore', stores[0].id);
    }
  }

  public logout(): void {
    const loggingOut = this.dialog.open(LogoutDialogComponent, {
      panelClass: 'no-padding-dialog',
    });
    loggingOut.afterClosed().subscribe(async (response) => {
      if (response) {
        localStorage.removeItem('token');
        await this.router.navigate(['login']);
      }
    });
  }

  isActive(sideBarItem: any): boolean {
    return sideBarItem.path === this.router.url;
  }

  //#region drawer functions
  public resetSidebar(): void {
    this.isSidebarOpened = false;
  }

  public setSidebarMode(): any {
    return window.innerWidth < 1100 ? 'over' : 'side';
  }

  public showMenuButton(): boolean {
    return window.innerWidth < 1100;
  }

  public toggleSidebar(drawer: MatDrawer): void {
    this.isSidebarOpened = true;

    drawer.toggle();
  }

  public showSidebar(): boolean {
    return window.innerWidth < 1100 ? this.isSidebarOpened : true;
  }

  //#endregion
  async action(sidebarItem: SideBarItem): Promise<void> {
    if (!sidebarItem.path) {
      this.dialog.open(TransactionComponent, {
        panelClass: 'no-padding-dialog',
      });
    } else {
      await this.router.navigate([sidebarItem.path]);
    }
  }

  userStore(): string {
    return this.user?.stores ? this.user.stores[0].legalName : '-';
  }
}
