import {
  Component,
  ChangeDetectionStrategy


} from '@angular/core';
import {
  CalendarEvent
} from 'angular-calendar';
import { EventColor } from 'calendar-utils';

const colors: Record<string, EventColor> = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'app-appointments',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.scss']
})
export class AppointmentsComponent {
  viewDate: Date = new Date();
  events: CalendarEvent[] = [
    {
      title: 'My event',
      start: new Date(),
      end: new Date(),
      color: colors.red
    }
  ];

  constructor() {
  }
}
