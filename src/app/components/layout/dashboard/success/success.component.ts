import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {
  innerHeight: number;
  constructor(public route: ActivatedRoute,private router: Router) {
    this.innerHeight = window.innerHeight - 100;
  }

  async ngOnInit(): Promise<void> {
    try {
      await this.getProductId();
    } catch (e) {
      console.log(e);
    }
  }

  async getProductId(): Promise<void> {
    return new Promise((resolve) => {
      this.route.params.subscribe(params => {
        console.log(params);
        resolve();
      });
    });
  }

  async navigateToHistory() {
    await this.router.navigate(['dashboard/account']);
  }
}
