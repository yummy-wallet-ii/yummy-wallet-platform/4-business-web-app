import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { LoadingService } from '../../../../../services/loading.service';
import { ToastMessagesService } from '../../../../../services/toast-messages.service';
import { OrdersService } from '../../../../../services/orders.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-orders-table',
  templateUrl: './orders-table.component.html',
  styleUrls: ['./orders-table.component.scss']
})
export class OrdersTableComponent implements OnInit {
  @Input() orders: any = [];
  @Input() isCompleted = false;
  @Output() reload: EventEmitter<any> = new EventEmitter<any>();
  tableHeight: number;
  displayColumns: string[] = [];
  constructor(
    public router: Router,
    private loadingService: LoadingService,
    private toastService: ToastMessagesService,
    private orderService: OrdersService
  ) {
    this.tableHeight = window.innerHeight - 200;


  }

  ngOnInit(): void {
    this.displayColumns = this.isCompleted ? ['orderUniqueId', 'orderDate', 'completedAt', 'price', 'actions'] :
      ['orderUniqueId', 'orderDate', 'price', 'actions'];
  }

  async preview(orderId: number): Promise<void> {
    await this.router.navigate([`dashboard/order-details/${orderId}`]);
  }

  async complete(orderId: number): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      await this.orderService.completeOrder(orderId);
      this.reload.emit();
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }
}
