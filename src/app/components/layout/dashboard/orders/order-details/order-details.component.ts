import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingService } from '../../../../../services/loading.service';
import { ToastMessagesService } from '../../../../../services/toast-messages.service';
import { OrdersService } from '../../../../../services/orders.service';
import { Product } from '../../../../../models/Product';
import { ProductsService } from '../../../../../services/products.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss'],
})
export class OrderDetailsComponent implements OnInit {
  orderId: number | undefined;
  displayedColumns = ['image', 'id', 'name', 'comments'];
  details: any;
  title = '';
  pageLoaded = false;
  items: any[] = [];
  tableHeight: number;
  isPending = false;
  constructor(
    public route: ActivatedRoute,
    public domSanitizer: DomSanitizer,
    public loadingService: LoadingService,
    public toastService: ToastMessagesService,
    private productService: ProductsService,
    private orderService: OrdersService
  ) {
    this.tableHeight = window.innerHeight - 250;
  }

  ngOnInit(): void {
    this.initializeComponent();
  }

  initializeComponent = async (): Promise<void> => {
    try {
      this.loadingService.setLoading(true);
      this.orderId = await this.getOrderId();
      this.details = await this.orderService.fetchOrderDetails(this.orderId);
      this.isPending = this.details.status !== 1;
      this.title = `Παραγγελία - ${this.details.orderUniqueId}`;
      this.items = await this.initProducts();
      console.log(this.items);
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
      this.pageLoaded = true;
    }
  };

  async getOrderId(): Promise<number> {
    return new Promise((resolve) => {
      this.route.params.subscribe((params) => {
        resolve(+params.id);
      });
    });
  }

  async initProducts(): Promise<any[]> {
    const products = [];
    for (const item of this.details.base_orderDetails) {
      const tempProduct: Product = await this.productService.getProduct(
        item.productId
      );
      const product = {
        id: tempProduct.id,
        image: `data:image/png;base64, ${await this.productService.fetchImage(
          item.productId
        )}`,
        name: tempProduct.title,
        comments: item.comments,
      };
      products.push(product);
    }

    return products;
  }

  async complete(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      await this.orderService.completeOrder(this.orderId);
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }
}
