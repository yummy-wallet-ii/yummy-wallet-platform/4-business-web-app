import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../../../../services/orders.service';
import { LoadingService } from '../../../../services/loading.service';
import { ToastMessagesService } from '../../../../services/toast-messages.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  orders: any[] = [];
  completedOrders: any[] = [];
  constructor(private orderService: OrdersService,
              private loadingService: LoadingService,
              private toastService: ToastMessagesService) { }

  async ngOnInit(): Promise<void> {
    await this.initOrders();
  }

  initOrders = async () => {
    try {
      this.loadingService.setLoading(true);
      this.orders = await this.orderService.fetchPending();
      this.completedOrders = await this.orderService.fetchCompleted();
      console.log(this.completedOrders);
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }
}
