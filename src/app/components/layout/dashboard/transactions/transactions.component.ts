import { Component, OnInit } from '@angular/core';
import { Transaction } from '../../../../models/Transaction';
import { LoadingService } from '../../../../services/loading.service';
import { ToastMessagesService } from '../../../../services/toast-messages.service';
import { TransactionsService } from '../../../../services/transactions.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss'],
})
export class TransactionsComponent implements OnInit {
  public searchAt: SearchPeriod[];
  private transactions: Transaction[] = [];
  public displayTransactions: Transaction[] = [];
  public displayedColumns = ['dateCreated', 'firstName', 'lastName', 'price'];
  private readonly storeId: number;
  public tableHeight: number;
  public selectedPeriod: SearchPeriod = {} as SearchPeriod;
  public displayRange: boolean;
  from: string;
  to: string;
  constructor(
    private loadingService: LoadingService,
    private toastService: ToastMessagesService,
    private transactionService: TransactionsService
  ) {
    this.from = '';
    this.to = '';
    this.displayRange = false;
    this.searchAt = [
      { id: 1, description: 'Σήμερα' },
      { id: 2, description: 'Χθές' },
      { id: 3, description: 'Για 1 μήνα' },
      { id: 4, description: 'Εύρος' },
    ];
    const storeId = localStorage.getItem('selectedStore');
    this.storeId = storeId ? +storeId : -1;
    this.selectedPeriod = this.searchAt[0];
    this.tableHeight = window.innerHeight - 270;
  }

  ngOnInit(): void {
    this.initializeComponent();
  }

  async initializeComponent(): Promise<void> {
    try {
      this.loadingService.setLoading(true);
      this.transactions = await this.transactionService.fetchAllTransactions(
        +this.storeId
      );
      console.log(this.transactions);
      this.displayTransactions = this.getTodayTransactions();
    } catch (e) {
      this.toastService.toastErrorMessage(e.message);
    } finally {
      this.loadingService.setLoading(false);
    }
  }

  search(): void {
    switch (this.selectedPeriod.id) {
      case 1:
        this.displayTransactions = this.getTodayTransactions();
        this.displayRange = false;
        break;
      case 2:
        this.displayTransactions = this.getYesterdayTransactions();
        this.displayRange = false;
        break;
      case 3:
        this.displayTransactions = this.getLastMonthTransactions();
        this.displayRange = false;
        break;
      case 4:
        this.displayRange = true;
        break;
    }
  }

  getTodayTransactions(): Transaction[] {
    const today = new Date();
    return this.transactions.filter(
      (transaction) => {
        const d1 = new Date(transaction.createdAt);
        const d2 = new Date(today.getFullYear(), today.getMonth(), today.getDate());
        const transactionDate = new Date(d1.getFullYear(), d1.getMonth(), d1.getDate());
        return transactionDate.getTime() === d2.getTime();
      }
    );
  }

  getYesterdayTransactions(): Transaction[] {
    let yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    yesterday = this.zeroDate(yesterday);
    return this.transactions.filter(
      (transaction) => {
        const d1 = new Date(transaction.createdAt);
        const d2 = new Date(yesterday.getFullYear(), yesterday.getMonth(), yesterday.getDate());
        const transactionDate = new Date(d1.getFullYear(), d1.getMonth(), d1.getDate());
        return transactionDate.getTime() === d2.getTime();
      }
    );
  }

  getLastMonthTransactions(): Transaction[] {
    const date = new Date();
    date.setMonth(date.getMonth() - 1);
    const lastMonth = this.zeroDate(date);
    return this.transactions.filter(
      (transaction) => {
        const d1 = this.zeroDate(new Date(transaction.createdAt));
        const d2 = this.zeroDate(lastMonth);
        return d1.getTime() > d2.getTime();
      }
    );
  }

  getCustomDateTransactions(): Transaction[] {
    const from = this.zeroDate(new Date(this.from));
    const to = this.zeroDate( new Date(this.to));
    return this.transactions.filter(
      (transaction) => {
        const d1 = new Date(transaction.createdAt);
        const fromDate = new Date(from.getFullYear(), from.getMonth(), from.getDate());
        const toDate = new Date(to.getFullYear(), to.getMonth(), to.getDate());
        const transactionDate = new Date(d1.getFullYear(), d1.getMonth(), d1.getDate());
        console.log(fromDate);
        console.log(toDate);
        console.log(d1);
        return (fromDate.getTime() <= transactionDate.getTime()) && (toDate.getTime() >= transactionDate.getDate());
      }
    );
  }

  zeroDate(date: Date): Date {
    const tempDate = date;
    tempDate.setHours(0);
    tempDate.setMinutes(0);
    tempDate.setSeconds(0);
    return tempDate;
  }

  searchBetweenDates(): any {
    if (this.from === '' || this.to === '') {
      this.toastService.toastMessages('Θα πρέπει να ορίσετε εύρος ημερομηνιών αναζήτησης');
      return;
    }

    this.displayTransactions = this.getCustomDateTransactions();
  }
}



interface SearchPeriod {
  id: number;
  description: string;
}
