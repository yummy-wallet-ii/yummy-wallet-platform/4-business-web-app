import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { NotificationsComponent } from './notifications/notifications.component';
import {FlexModule} from "@angular/flex-layout";
import {MatCardModule} from "@angular/material/card";
import { SummaryComponent } from './summary/summary.component';
import { SharedModule } from '../../shared/shared.module';
import { MatRadioModule } from '@angular/material/radio';


@NgModule({
  declarations: [
    ProfileComponent,
    NotificationsComponent,
    SummaryComponent
  ],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    FlexModule,
    MatCardModule,
    SharedModule,
    MatRadioModule
  ]
})
export class ProfileModule { }
