import { Component, OnInit } from '@angular/core';
import { Transaction } from '../../../../models/Transaction';
import { TransactionsService } from '../../../../services/transactions.service';
import { ToastMessagesService } from '../../../../services/toast-messages.service';
import {StoreService} from "../../../../services/store.service";

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
})
export class SummaryComponent implements OnInit {
  summary = [];
  transactions: Transaction[];
  transactionsToday: Transaction[];
  cardFlex = 23;
  storeId: number | null;
  store: any;
  constructor(
    private transactionService: TransactionsService,
    public toastMessagesService: ToastMessagesService,
    private storeService: StoreService,
  ) {
    this.transactions = [];
    this.transactionsToday = [];
    this.storeId = localStorage.getItem('selectedStore')
      ? // @ts-ignore
        +localStorage.getItem('selectedStore')
      : null;
  }


  async ngOnInit(): Promise<void> {
    try {
      this.store = await this.storeService.fetchStore(this.storeId);
      console.log(this.store);
      this.transactions = await this.fetchTransactions();
      this.transactionsToday = this.transactionService.todayTransactions(
        this.transactions
      );
      console.log(this.transactions);
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    }
  }

  async fetchTransactions(): Promise<Transaction[]> {
    return await this.transactionService.fetchAllTransactions(this.storeId);
  }

  totalTransactions(): number {
    return this.transactions.length;
  }
  todayTransactions(): number {
    return this.transactionsToday.length;
  }

  totalTransactionsPrice(): number {
    let sum = 0;
    for (const transaction of this.transactions) {
      sum += transaction.price;
    }
    return sum;
  }

  totalTodayTransactionsPrice(): number {
    let sum = 0;
    for (const transaction of this.transactionsToday) {
      sum += transaction.price;
    }
    return sum;
  }

  totalCustomers(): number {
    const customers: any[] = [];
    for (const transaction of this.transactions) {
      const exist = customers.find(
        (customer) => customer === transaction.customerId
      );
      if (!exist) {
        customers.push(transaction.customerId);
      }
    }
    return customers.length;
  }
}
