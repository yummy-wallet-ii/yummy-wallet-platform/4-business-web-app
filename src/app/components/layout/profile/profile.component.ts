import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoadingService } from '../../../services/loading.service';
import { ToastMessagesService } from '../../../services/toast-messages.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  loading = true;
  subscription: Subscription;
  constructor(private loadingService: LoadingService,
              private toastMessagesService: ToastMessagesService
  ) {
    this.subscription = this.loadingService.observe().subscribe(state => {
        this.loading = state;
      },
      error => this.toastMessagesService.toastErrorMessage(error.message));
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
