import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { ExtendedModule, FlexModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { AgmCoreModule } from '@agm/core';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { MatDividerModule } from '@angular/material/divider';
import { MatTableModule } from '@angular/material/table';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { MatTabsModule } from '@angular/material/tabs';
import { GeofenceModule } from './geofence/geofence.module';

@NgModule({
  declarations: [SettingsComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    FlexModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBm3F10SsOEKML3c3XYjv2Wouoenx7hK0w'
    }),
    MatSlideToggleModule,
    GooglePlaceModule,
    MatDividerModule,
    MatTableModule,
    NgxMaterialTimepickerModule,
    FormsModule,
    ExtendedModule,
    MatTabsModule,
    GeofenceModule
  ]
})
export class SettingsModule {}
