import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../../dialogs/confirm-dialog/confirm-dialog.component';
import { ToastMessagesService } from '../../../services/toast-messages.service';
import { FileService } from '../../../services/file.service';
import { StoreService } from '../../../services/store.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '../../../models/Store';
import { MatTableDataSource } from '@angular/material/table';

interface Marker {
  lat: number;
  lng: number;
}
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  @ViewChild('fileInput', { static: false }) fileInput: ElementRef | undefined;
  @ViewChild('imageElement', { static: false }) imageElement:
    | ElementRef
    | undefined;
  imageSource: any;
  selectedFile: any;
  storeId: number | null;
  loadingImage: boolean;
  imageUploading: boolean;
  form: FormGroup;
  form2: FormGroup;
  lat = 40.644367;
  lng = 22.926226;
  markers: Marker[];
  store: Store | undefined;
  dataSource!: MatTableDataSource<any>;
  schedule: string | undefined;
  displayedColumns: any[];

  constructor(
    private readonly formBuilder: FormBuilder,
    public dialog: MatDialog,
    private storeService: StoreService,
    private fileService: FileService,
    private toastMessagesService: ToastMessagesService
  ) {
    this.displayedColumns = ['day', 'start', 'end'];
    this.markers = [];
    this.loadingImage = false;
    this.imageUploading = false;
    this.imageSource = '';
    this.storeId = localStorage.getItem('selectedStore')
      ? // @ts-ignore
        +localStorage.getItem('selectedStore')
      : null;
    this.form2 = this.formBuilder.group({
      descriptionEl: [this.store ? this.store.descriptionEl : ''],
      descriptionEn: [this.store ? this.store.descriptionEn : ''],
      postalCode: [
        this.store ? this.store.postalCode : '',
        [Validators.required, Validators.pattern('^[0-9]*$')],
      ],
      street: [this.store ? this.store.street : '', Validators.required],
      telephone: [
        this.store ? this.store.telephone : '',
        [Validators.required, Validators.pattern('^[0-9]*$')],
      ],
      website: [this.store ? this.store.website : ''],
      facebook: [this.store ? this.store.facebook : ''],
      instagram: [this.store ? this.store.instagram : ''],
      twitter: [this.store ? this.store.twitter : ''],
      legalName: [this.store ? this.store.legalName : '', Validators.required],
      headquarters: [
        this.store ? this.store.headquarters : '',
        Validators.required,
      ],
      taxLegalName: [
        this.store ? this.store.taxLegalName : '',
        Validators.required,
      ],
      taxProfession: [
        this.store ? this.store.taxProfession : '',
        Validators.required,
      ],
      taxNumber: [
        this.store ? this.store.taxNumber : '',
        [Validators.required, Validators.pattern('^[0-9]*$')],
      ],
      taxOffice: [this.store ? this.store.taxOffice : '', Validators.required],
      vat: [this.store ? this.store.vat : '', Validators.required],
      schedule: [[]],
    });
    this.form = this.formBuilder.group({
      customerPercentage: [0],
      yummyPercentage: [0],
      promotedCategory: [0],
      storePercentage: [0],
      cashbackPercentage: [0],
      latitude: [0.0],
      longitude: [0.0],
      stamps: [false],
      cashBack: [false],
      iBankPay: [false],
    });
  }

  /**
   * Fetches and initializes store info.
   * Parses the schedule so that it can be displayed properly.
   * Fetches and initializes store options.
   */
  async ngOnInit(): Promise<void> {
    try {
      await this.fetchStoreFrontImage();
      this.store = await this.fetchStoreInfo();
      console.log(this.store);
      this.dataSource = new MatTableDataSource<any>(
        JSON.parse(this.store.schedule)
      );
      this.form2 = this.storeService.initStoreInfoForm(this.store);
      await this.fetchStoreDetails();
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    } finally {
      this.loadingImage = false;
    }
  }

  async fetchStoreInfo(): Promise<Store> {
    return await this.storeService.fetchStore(this.storeId);
  }

  async fetchStoreDetails(): Promise<void> {
    const options = await this.storeService.fetchStoreDetails(this.storeId);
    this.form = this.storeService.initStoreOptionsForm(options);

    if (options && options.latitude && options.longitude) {
      const marker: Marker = {
        lat: +options.latitude,
        lng: +options.longitude,
      };
      this.setMarker(marker);
    }
  }

  async fetchStoreFrontImage(): Promise<void> {
    this.loadingImage = true;
    try {
      const res = await this.fileService.downloadFile(
        `stores/frontImage/${this.storeId}`
      );
      const reader = new FileReader();
      reader.readAsDataURL(res);
      reader.onloadend = () => {
        this.imageSource = reader.result;
      };
      reader.onerror = () => {
        this.imageSource = '';
      };
    } catch (e) {
      this.imageSource = '';
    }
  }

  removeImage(): void {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        icon: 'delete',
        title: 'Διαγραφή εικόνας',
        message:
          'Είστε σίγουροι ότι θέλετε να διαγράψετε την εικόνα του καταστήματος?',
      },
    });

    confirmDialog.afterClosed().subscribe(async (result: any) => {
      if (result) {
        try {
          this.imageUploading = true;
          await this.storeService.removeStoreImage(this.storeId);
          this.imageUploading = false;
          this.toastMessagesService.toastMessages(
            'Η διαγραφή οληκλώθηκε με επιτυχία'
          );
          this.imageSource = '';
        } catch (e) {
          this.toastMessagesService.toastErrorMessage(e.message);
        }
      }
    });
  }

  confirmUploadImage(): void {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        icon: 'add',
        title: 'Μεταφόρτωση εικόνας',
        message: 'Θέλετε να ανεβάσετε το λογότυπο αυτό?',
      },
    });

    confirmDialog.afterClosed().subscribe(async (result: any) => {
      if (result) {
        try {
          this.imageUploading = true;
          await this.fileService.uploadFile(
            `stores/frontImage?storeId=${this.storeId}`,
            this.selectedFile
          );
          this.imageUploading = false;
          this.toastMessagesService.toastMessages(
            'Η μεταφόρτωση οληκλώθηκε με επιτυχία'
          );
        } catch (e) {
          this.toastMessagesService.toastErrorMessage(e.message);
        }
      } else {
        this.imageSource = '';
        // @ts-ignore
        this.imageElement.nativeElement.src = '';
      }
    });
  }

  onFileInput(): void {
    // @ts-ignore
    const file: File = this.fileInput.nativeElement.files[0];
    this.selectedFile = file;
    const reader = new FileReader();
    reader.onload = (e) => {
      // this.imageElement.nativeElement.src = reader.result;
      const image = new Image();
      // @ts-ignore
      if (typeof e.target.result === 'string') {
        // @ts-ignore
        image.src = e.target.result;
      }

      // @ts-ignore
      image.onload = () => {
        const height = image.height;
        const width = image.width;
        if (file.type !== 'image/png' && file.type !== 'image/jpeg') {
          this.toastMessagesService.toastErrorMessage(
            `Μη αποδεκτή μορφή εικόνας`
          );
          return false;
        }
        if (height > 5000 || width > 5000) {
          this.toastMessagesService.toastErrorMessage(
            `Η φωτογραφία θα πρέπει να έχει μέγιστες διαστάσεις 300px ύψος και 300px πλάτος`
          );
          return false;
        } else {
          this.imageSource = reader.result;
        }
      };
    };
    reader.readAsDataURL(file);
  }

  initMarkerLocationForm(marker: any): void {
    this.form.controls.latitude.setValue(marker.lat);
    this.form.controls.longitude.setValue(marker.lng);
  }

  handleAddressChange($event: any): any {
    const marker: Marker = {
      lat: +$event.geometry.location.lat(),
      lng: +$event.geometry.location.lng(),
    };

    this.setMarker(marker);
  }

  async updateForm(): Promise<void> {
    try {
      const model = {
        storeInfo: this.form2.getRawValue(),
        storeOptions: this.form.getRawValue(),
      };
      await this.storeService.updateStore(model, this.store?.id);
      this.toastMessagesService.toastMessages(
        `Το κατάστημά σας ενημερώθηκε με επιτυχία`
      );
    } catch (e) {
      console.log(e);
      this.toastMessagesService.toastErrorMessage(e.message);
    }
  }

  setMarker(marker: Marker): void {
    this.initMarkerLocationForm(marker);
    this.lat = marker.lat;
    this.lng = marker.lng;
    if (this.markers.length > 0) {
      this.markers[0] = marker;
    } else {
      this.markers.push(marker);
    }
  }

  async updateSchedule(): Promise<void> {
    try {
      const model = {
        storeInfo: {schedule: JSON.stringify(this.dataSource.data)},
        storeOptions: {},
      };
      await this.storeService.updateStore(model, this.store?.id);
      this.toastMessagesService.toastMessages(
        `Το ωράριο του καταστήματος σας ενημερώθηκε με επιτυχία`
      );
    } catch (e) {
      console.log(e);
      this.toastMessagesService.toastErrorMessage(e.message);
    }
  }
}
