import { Component, Input, OnInit } from '@angular/core';
import { Store } from '../../../../models/Store';
import { FormBuilder, FormGroup } from '@angular/forms';
import { StoreService } from '../../../../services/store.service';
import { ToastMessagesService } from '../../../../services/toast-messages.service';

@Component({
  selector: 'app-geofence',
  templateUrl: './geofence.component.html',
  styleUrls: ['./geofence.component.scss']
})
export class GeofenceComponent implements OnInit {
  @Input() store: Store | undefined;
  @Input() latitude: number = 0;
  @Input() longitude: number = 0;
  windowWidth: number;
  windowHeight: number;
  form: FormGroup;
  geofenceTitle: string;
  geofenceBody: string;
  mapOptions: google.maps.MapOptions = {
    draggable: false
  };
  constructor( private readonly formBuilder: FormBuilder,
               private storeService: StoreService,
               public toastMessagesService: ToastMessagesService) {
    this.windowWidth = window.innerWidth /3;
    this.windowHeight = window.innerHeight - 270;
    this.geofenceBody = '';
    this.geofenceTitle = '';


    this.form = this.formBuilder.group({
      title: [this.geofenceTitle ? this.geofenceTitle : ''],
      body: [this.geofenceBody ? this.geofenceBody : ''],
      isActive: [this.geofenceBody ? this.geofenceBody : ''],
    });
  }

  ngOnInit(): void {
  }

  async updateGeoFence(): Promise<void> {
      try {
        // @ts-ignore
        await this.storeService.updateStoreGeoFence(this.form.getRawValue(), this.store.id);
        this.toastMessagesService.toastMessages(
          `Το κατάστημά σας ενημερώθηκε με επιτυχία`
        );
      } catch (e) {
        console.log(e);
        this.toastMessagesService.toastErrorMessage(e.message);
      }
  }
}
