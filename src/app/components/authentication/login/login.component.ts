import { Component, OnInit } from '@angular/core';
import { BusinessLoginModel } from '../../../models/Business';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication.service';
import { ToastMessagesService } from '../../../services/toast-messages.service';
import { LoadingService } from '../../../services/loading.service';
import { GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginModel: BusinessLoginModel = { identifier: '', password: '' };
  form: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    public router: Router,
    private socialAuthService: SocialAuthService,
    private loadingService: LoadingService,
    public toastMessagesService: ToastMessagesService,
    private authenticationService: AuthenticationService
  ) {
    this.form = this.formBuilder.group({
      identifier: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  ngOnInit(): void {}

  /**
   * A user logs in with their email and password.
   */
  login = async () => {
    if (this.form.valid) {
      this.loadingService.setLoading(true);
      this.loginModel = this.form.getRawValue();
      try {
        const token = await this.authenticationService.login(this.loginModel);
        console.log(token);
        localStorage.setItem('token', token);
        await this.router.navigate(['dashboard']);
      } catch (e) {
        console.log(e);
        this.toastMessagesService.toastErrorMessage(e.message);
      } finally {
        this.loadingService.setLoading(false);
      }
    } else {
      console.log('There is a problem with the form');
    }
  };

  navigateToRegistration = async () => {
    localStorage.removeItem('user');
    await this.router.navigate(['registration']);
  };

  /**
   * A user logs in with their Google account.
   * If they are not registered, they are redirected to the registration page. Their email is prefilled in the form.
   */
  async loginWithGoogle(): Promise<void> {
    try {
      const result = await this.socialAuthService.signIn(
        GoogleLoginProvider.PROVIDER_ID
      );

      const googleUser = {
        provider: result.provider,
        email: result.email,
      };
      const serverResult = await this.authenticationService.externalLogin({
        identifier: result.email,
        provider: result.provider,
      });

      if (serverResult && serverResult.token) {
        localStorage.setItem('token', serverResult.token);
        await this.router.navigate(['dashboard']);
      } else {
        localStorage.setItem('user', JSON.stringify(googleUser));
        await this.router.navigate(['registration']);
      }
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    }
  }
}
