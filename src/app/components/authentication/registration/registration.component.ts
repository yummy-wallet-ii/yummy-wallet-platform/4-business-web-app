import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication.service';
import { MatTableDataSource } from '@angular/material/table';
import { ToastMessagesService } from '../../../services/toast-messages.service';
import { GeneralService } from '../../../services/general.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {
  cities: any;
  countries: any;
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[];
  schedule: any[];
  form: FormGroup;
  isExternalUser: boolean;
  user: any;
  categories: any;

  constructor(
    private readonly formBuilder: FormBuilder,
    public router: Router,
    private authenticationService: AuthenticationService,
    private generalService: GeneralService,
    public toastMessagesService: ToastMessagesService
  ) {
    this.isExternalUser = false;
    this.categories = [];
    try {
      const jsonUser = localStorage.getItem('user');
      if (jsonUser) {
        this.user = JSON.parse(jsonUser);
        this.isExternalUser = true;
      }
    } catch (e) {
      console.log(`no user in localstorage`);
    }
    this.displayedColumns = ['day', 'start', 'end'];
    // todo: fetch countries when the server is ready
    this.countries = ['Ελλάδα'];
    this.schedule = [
      {
        day: 'Δευτέρα',
        start: '00:00',
        end: '00:00',
      },
      {
        day: 'Τρίτη',
        start: '00:00',
        end: '00:00',
      },
      {
        day: 'Τετάρτη',
        start: '00:00',
        end: '00:00',
      },
      {
        day: 'Πέμπτη',
        start: '00:00',
        end: '00:00',
      },
      {
        day: 'Παρασκευή',
        start: '00:00',
        end: '00:00',
      },
      {
        day: 'Σάββατο',
        start: '00:00',
        end: '00:00',
      },
      {
        day: 'Κυριακή',
        start: '00:00',
        end: '00:00',
      },
    ];
    this.dataSource = new MatTableDataSource<any>(this.schedule);

    this.form = this.formBuilder.group({
      cityId: ['', Validators.required],
      countryId: ['', Validators.required],
      categoryId: ['', Validators.required],
      descriptionEl: [''],
      descriptionEn: [''],
      postalCode: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      street: ['', Validators.required],
      email: [
        this.isExternalUser ? this.user.email : '',
        [Validators.required, Validators.email],
      ],
      telephone: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      website: [''],
      facebook: [''],
      instagram: [''],
      twitter: [''],
      legalName: ['', Validators.required],
      headquarters: ['', Validators.required],
      taxLegalName: ['', Validators.required],
      taxProfession: ['', Validators.required],
      taxNumber: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      taxOffice: ['', Validators.required],
      vat: ['', Validators.required],
      schedule: [[]],
      provider: [this.isExternalUser ? this.user.provider : 'local'],
    });

    // If a user doesn't register with their Google account, password is needed.
    if (!this.isExternalUser) {
      this.form.addControl(
        'password',
        new FormControl('', Validators.required)
      );
    }
  }

  /**
   * Responsive layout for the form.
   */
  fieldFlex = () => {
    return this.isExternalUser ? 32 : 49;
  }

  async ngOnInit(): Promise<void> {
    await this.getCities();
    await this.getCategories();
  }

  /**
   * Fetches the cities.
   */
  getCities = async () => {
    try {
      this.cities = await this.generalService.getCities();
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    }
  }


  /**
   * Fetches the categories.
   */
  getCategories = async () => {
    try {
      this.categories = await this.generalService.fetchAllBusinessCategories();
    } catch (e) {
      this.toastMessagesService.toastErrorMessage(e.message);
    }
  }

  /**
   * Registers the store and navigates to login.
   */
  register = async () => {
    // adding schedule to the form
    for (const row of this.dataSource.data) {
      this.form.value.schedule.push(row);
    }
    try {
      await this.authenticationService.register(this.form.getRawValue());
      this.toastMessagesService.toastMessages(`Το κατάστημα έχει εγγραφεί επιτυχώς`);
      await this.navigateToLogin();
    } catch (e) {
      console.log(e);
      this.toastMessagesService.toastErrorMessage(e.message);
    }
  }

  navigateToLogin = async () => {
    localStorage.removeItem('user');
    await this.router.navigate(['login']);
  }
}
