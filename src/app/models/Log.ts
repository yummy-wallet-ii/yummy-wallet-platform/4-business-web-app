export interface Log {
  logId: number;
  message: string;
  createdAt: Date;
  actionId: number;
  actionDescription: string;
  typeId: number;
  typeDescription: string;
  userId: number;
  identifier: string;
  storeId: number;
}
