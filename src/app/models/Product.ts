export interface Product {
  id: number;
  title: string;
  hasDiscount: boolean;
  image?: string | undefined;
  imageBlob?: string | undefined;
  description: string;
  categoryId: number;
  price: number;
  alternativePrice?: number;
  storeId: number | undefined | null;
}
