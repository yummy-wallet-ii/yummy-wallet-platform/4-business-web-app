export interface Transaction {
  id: number;
  userId: number;
  userName: string;
  storeId: number;
  price: number;
  customerId: number;
  customerName: string;
  dateCreated: string;
  createdAt: Date;
}
