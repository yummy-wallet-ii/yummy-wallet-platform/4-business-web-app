export interface Category {
  id: number;
  name: string;
  description: string;
  storeId?: number;
  image?: string;
  imageBlob?: string;
  hasStamps?: boolean;
  stampsRewards?: number;
}
