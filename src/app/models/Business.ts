export interface BusinessLoginModel {
  identifier: string;
  password: string;
}

export interface BusinessRegistrationModel {
  email: string;
  password: string;
  businessName: string;
  country: string;
  city: string;
  postalCode: number;
  telephone: string;
  createdAt?: string;
  modifiedAt?: string;
}
