export interface TableColumn {
  label: string;
  key: string;
  type: string;
  displayOnTable: boolean;
}
