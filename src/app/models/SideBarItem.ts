export interface SideBarItem {
  title: string;
  path?: string;
  openDialog: boolean;
  icon?: string;
  hasBadge?: boolean
}
