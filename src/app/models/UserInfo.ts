export interface UserInfo {
  id: number;
  identifier: string;
  photoUrl?: string;
}
