export interface Store {
  id: number;
  cityId: number;
  countryId: number;
  descriptionEl: string;
  descriptionEn: string;
  mapPreview: boolean;
  postalCode: string;
  street: string;
  email: string;
  telephone: string;
  website: string;
  facebook: string;
  instagram: string;
  twitter: string;
  legalName: string;
  headquarters: string;
  taxLegalName: string;
  taxProfession: string;
  taxNumber: string;
  taxOffice: string;
  vat: number;
  categoryId: number;
  schedule: string;
}
