import {Injectable} from '@angular/core';
import {BusinessLoginModel} from '../models/Business';
import {CommonService} from './common.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private commonService: CommonService) {
  }

  login = async (loginData: BusinessLoginModel): Promise<any> => {
    try {
      return await this.commonService.postRequest('users/login', loginData);
    } catch (e) {
      throw e;
    }
  }

  externalLogin = async (loginData: any): Promise<any> => {
    const {identifier, provider} = loginData;
    const result = await this.commonService.postRequest('users/externalLogin', {identifier, provider});
    console.log('got result');
    console.log(result);
    return result;
  }


  register = async (registerData: any): Promise<any> => {
    try {
      return await this.commonService.postRequest('stores', registerData);
    } catch (e) {
      throw e;
    }
  }
}
