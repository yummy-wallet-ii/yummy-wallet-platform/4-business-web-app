import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  pendingOrdersSubject: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  setPendingOrders(state: number): void {
    this.pendingOrdersSubject.next(state);
  }

  observe(): Observable<number> {
    return this.pendingOrdersSubject.asObservable();
  }
  constructor(private commonService: CommonService) { }

  async fetchPending(): Promise<any> {
    const orders = await this.commonService.getRequest('orders/pending');
    this.setPendingOrders(orders.length);
    return orders;
  }

  async fetchCompleted(): Promise<any> {
    return await this.commonService.getRequest('orders/completed');
  }

  async fetchOrderDetails(orderId: number | undefined): Promise<any> {
    return await this.commonService.getRequest(`orders/${orderId}`);
  }

  async completeOrder(orderId: number | undefined): Promise<any> {
    return await this.commonService.putRequest(`orders/complete/${orderId}`, {});
  }
}
