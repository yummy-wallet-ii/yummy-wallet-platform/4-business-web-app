import { Injectable } from '@angular/core';
import { Notification } from '../models/Notification';
import { CommonService } from './common.service';
import { BehaviorSubject } from 'rxjs';
import { io } from 'socket.io-client';
import { ToastMessagesService } from './toast-messages.service';
import { OrdersService } from './orders.service';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class NotificationsService {
  socketSubject: BehaviorSubject<boolean>;
  storeId: string | null;
  socket: any;
  constructor(
    private commonService: CommonService,
    public router: Router,
    private orderService: OrdersService,
    public toastService: ToastMessagesService
  ) {
    this.storeId = localStorage.getItem('selectedStore');
    this.socketSubject = new BehaviorSubject<boolean>(false);
  }

  async sendNotification(
    userTokens: string[],
    notification: Notification,
    storeId: number | null | undefined
  ): Promise<void> {
    const postBody = {
      recipients: userTokens,
      storeId,
      notification,
    };

    return await this.commonService.postRequest('messages', postBody);
  }

  initSocketCommunication(): void {
    this.socket = io(environment.socketUrl, {
      reconnectionDelay: 1000,
      reconnection: true,
      transports: ['websocket'],
      agent: false,
      upgrade: false,
      rejectUnauthorized: false,
    });

    this.socket.on('connect', () => {
      console.log(this.socket.id); // x8WIv7-mJelg7on_ALbx
    });

    this.socket.on('disconnect', () => {
      console.log('disconnecting from socket');
      console.log(this.socket.id); // undefined
    });

    this.socket.on(`orders-${this.storeId}`, async () => {
      this.toastService.toastMessages('Εχετε νέες παραγγελίες');
      await this.orderService.fetchPending();
    });
  }

  async navigateToOrders(): Promise<void> {
    await this.router.navigate(['dashboard/orders']);
  }
  getMessage(): any {}
}
