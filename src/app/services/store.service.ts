import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import { Store } from '../models/Store';
import { FormBuilder, Validators } from '@angular/forms';
import { Log } from '../models/Log';

@Injectable({
  providedIn: 'root',
})
export class StoreService {
  constructor(
    private commonService: CommonService,
    private readonly formBuilder: FormBuilder
  ) {}

  getUserStores = async (): Promise<any> => {
    return await this.commonService.getRequest('stores/userStores');
  }

  fetchStore = async (storeId: number | null): Promise<Store> => {
    return await this.commonService.getRequest(`stores/${storeId}`);
  }

  fetchRefills = async (storeId: number | null):Promise<any> => {
    return await this.commonService.getRequest(`stores/refills/${storeId}`);
  }

  updateStore = async (
    model: any,
    storeId: number | undefined
  ): Promise<any> => {
    return await this.commonService.putRequest(`stores/${storeId}`, model);
  }

  updateStoreGeoFence = async (
    model: any,
    storeId: number | undefined
  ): Promise<any> => {
    return await this.commonService.putRequest(`stores/geofence/${storeId}`, model);
  }

  removeStoreImage = async (storeId: number | null): Promise<any> => {
    return await this.commonService.deleteRequest(
      `stores/frontImage/${storeId}`
    );
  }

  fetchStoreDetails = async (storeId: number | null): Promise<any> => {
    return await this.commonService.getRequest(`stores/options/${storeId}`);
  }

  initStoreOptionsForm(options: any): any {
    return this.formBuilder.group({
      customerPercentage: [options ? options.customerPercentage : 0],
      yummyPercentage: [options ? options.yummyPercentage : 0],
      promotedCategory: [options ? options.promotedCategory : 0],
      storePercentage: [options ? options.storePercentage : 0],
      cashbackPercentage: [options ? options.cashbackPercentage : 0],
      latitude: [options ? options.latitude : 0],
      longitude: [options ? options.longitude : 0],
      stamps: [options ? options.stamps : false],
      cashBack: [options ? options.cashBack : false],
      iBankPay: [options ? options.iBankPay : false],
    });
  }

  initStoreInfoForm(store: Store): any {
    return this.formBuilder.group({
      descriptionEl: [store ? store.descriptionEl : ''],
      descriptionEn: [store ? store.descriptionEn : ''],
      postalCode: [
        store ? store.postalCode : '',
        [Validators.required, Validators.pattern('^[0-9]*$')],
      ],
      street: [store ? store.street : '', Validators.required],
      telephone: [
        store ? store.telephone : '',
        [Validators.required, Validators.pattern('^[0-9]*$')],
      ],
      website: [store ? store.website : ''],
      facebook: [store ? store.facebook : ''],
      instagram: [store ? store.instagram : ''],
      twitter: [store ? store.twitter : ''],
      legalName: [store ? store.legalName : '', Validators.required],
      headquarters: [store ? store.headquarters : '', Validators.required],
      taxLegalName: [store ? store.taxLegalName : '', Validators.required],
      taxProfession: [store ? store.taxProfession : '', Validators.required],
      taxNumber: [
        store ? store.taxNumber : '',
        [Validators.required, Validators.pattern('^[0-9]*$')],
      ],
      taxOffice: [store ? store.taxOffice : '', Validators.required],
      vat: [store ? store.vat : '', Validators.required],
      schedule: [store ? store.schedule : ''],
    });
  }

  async fetchStoreLogs(storeId: number | null): Promise<Log[]> {
    return await this.commonService.getRequest(`stores/${storeId}/logs`);
  }

  async refill(storeId: number | undefined | null, body: any) {
    return await this.commonService.postRequest(`stores/refills/${storeId}`, body);
  }
}
