import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  constructor(private http: HttpClient) {
  }

  customUpload(formData: any, model: any): any {
    return this.http.post(
      `files/upload-custom-form-files?uniqueId=${model.uniqueId}&childId=${model.childId}`,
      formData,
      {
        reportProgress: true,
        observe: 'events'
      }
    );
  }

  customParentUpload(formData: any, model: any): any {
    return this.http.post(
      `files/upload-custom-form-files-parent?uniqueId=${model.uniqueId}&parentId=${model.parentId}`,
      formData,
      {
        reportProgress: true,
        observe: 'events'
      }
    );
  }

  uploadFile(url: any, file: any): any {
    const formData = new FormData();
    formData.append('file', file);
    return new Promise<void>((resolve, reject) => {
      this.http.post(`${url}`, formData).subscribe(
        () => {
          resolve();
        },
        (err) => reject(err)
      );
    });
  }

  downloadFile(url: string): any {
    return new Promise((resolve, reject) => {
      this.http.get(url, { responseType: 'blob' }).subscribe(
        (res) => resolve(res),
        (error) => reject(error)
      );
    });
  }

  async getFile(url: string, path: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(url, { path }, { responseType: 'blob' }).subscribe(
        (res) => resolve(res),
        (error) => reject(error)
      );
    });
  }
}
