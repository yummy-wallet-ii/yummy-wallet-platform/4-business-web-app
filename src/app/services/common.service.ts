import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { chain, find, cloneDeep } from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  constructor(private httpClient: HttpClient) {
  }

  getRequest = async (endpoint: string): Promise<any> => {
    return new Promise((resolve, reject) => {
      this.httpClient.get(endpoint).subscribe(
        (result) => {
          resolve(result);
        },
        (error) => reject(error)
      );
    });
  }

  postRequest = async (endpoint: string, body: any): Promise<any> => {
    return new Promise((resolve, reject) => {
      this.httpClient.post(endpoint, body).subscribe(
        (result) => {
          resolve(result);
        },
        (error) => reject(error)
      );
    });
  }

  putRequest = async (endpoint: string, body: any): Promise<any> => {
    return new Promise((resolve, reject) => {
      this.httpClient.put(endpoint, body).subscribe(
        (result) => {
          resolve(result);
        },
        (error) => reject(error)
      );
    });
  }

  deleteRequest = async (endpoint: string): Promise<any> => {
    return new Promise((resolve, reject) => {
      this.httpClient.delete(endpoint).subscribe(
        (result) => {
          resolve(result);
        },
        (error) => reject(error)
      );
    });
  }

  find = (array: any, searchKey: any, searchValue: any) => {
    const search: any = {};
    search[searchKey] = searchValue;
    return find(array, {categoryId: searchValue});
  }

  deepClone = (obj: any) => cloneDeep(obj);

  group = (array: any, groupKey: string, params?: any) =>  chain(array)
    .groupBy(x => x[groupKey])
    .map((value, key) => ({categoryId: key, params, products: value}))
    .value()
}
