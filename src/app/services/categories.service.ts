import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import { Category } from '../models/Category';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  categories: Category[];
  constructor(private commonService: CommonService) {
    this.categories = [
      {id: 1, name: 'Coffee', description: 'Καφές', storeId: 1},
      {id: 2, name: 'Food', description: 'Φαγητά', storeId: 1},
      {id: 3, name: 'Beverages', description: 'Αναψυκτικά', storeId: 1},
      {id: 4, name: 'Snacks', description: 'Σνακς', storeId: 1}
    ]
  }

  // Create a new category
  async createCategory(category: any, storeId: number): Promise<any> {
    return await this.commonService.postRequest(`categories?storeId=${storeId}`, category);
  }

  // Get all categories
  async getCategories(): Promise<Category[]> {
    return await this.commonService.getRequest('categories');
    // return Promise.resolve(this.categories);
  }

  // Get category's Image
  async fetchCategoryImage(categoryId: number): Promise<any> {
    return await this.commonService.getRequest(`categories/fetchCategoryImage/${categoryId}`);
  }

  // Update an existing category
  async updateCategory(category: Category): Promise<any> {
    return await this.commonService.putRequest(`categories`, category);
  }

  async updateImage(formData: any, storeId: number): Promise<any> {
    return await this.commonService.postRequest(`categories/updateImage?storeId=${storeId}`, formData);
  }

  // Delete a category
  async deleteCategory(id: number): Promise<any> {
    return await this.commonService.deleteRequest(`categories/${id}`);
  }
}
