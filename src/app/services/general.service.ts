import {Injectable} from '@angular/core';
import {CommonService} from './common.service';

@Injectable({
  providedIn: 'root',
})
export class GeneralService {
  constructor(private commonService: CommonService) {
  }

  getCities = async (): Promise<any> => {
    try {
      return await this.commonService.getRequest('cities');
    } catch (e) {
      throw e;
    }
  }

  getCategories = async (): Promise<any> => {
    try {
      return await this.commonService.getRequest('categories');
    } catch (e) {
      throw e;
    }
  }

  fetchAllBusinessCategories = async (): Promise<any> => {
    return await this.commonService.getRequest('categories/business');
  }
}
