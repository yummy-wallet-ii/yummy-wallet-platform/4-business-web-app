import { Injectable } from '@angular/core';
import { Product } from '../models/Product';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private commonService: CommonService) {
  }

  // Create a new product
  async createProduct(formData: any, storeId: number): Promise<any> {
    return await this.commonService.postRequest(`products?storeId=${storeId}`, formData);
  }

  // Create a new product
  async updateProductImage(formData: any, storeId: number): Promise<any> {
    return await this.commonService.postRequest(`products/updateImage?storeId=${storeId}`, formData);
  }

  // Get all products
  async getProducts(storeId: number): Promise<Product[]> {
    return await this.commonService.getRequest(`products?storeId=${storeId}`);
  }

  // Get a single product by ID
  async getProduct(id: number): Promise<Product> {
    return await this.commonService.getRequest(`products/${id}`) as Product;
  }

  // Get product's Image
  async fetchImage(productId: number): Promise<any> {
    return await this.commonService.getRequest(`products/${productId}/image`);
  }

  // Update an existing product
  async updateProduct(product: any): Promise<any> {
    return await this.commonService.putRequest(`products`, product);
  }

  // Delete a product
  async deleteProduct(id: number | undefined): Promise<any> {
    return await this.commonService.deleteRequest(`products/${id}`);
  }
}
