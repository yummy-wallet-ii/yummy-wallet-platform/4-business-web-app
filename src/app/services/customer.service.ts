import { Injectable } from '@angular/core';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private commonService: CommonService) { }

  fetchAll = async () => {
    return await this.commonService.getRequest(`customers/store/`);
  }
}
