import { Injectable } from '@angular/core';
import { CommonService } from './common.service';
import { Transaction } from '../models/Transaction';

@Injectable({
  providedIn: 'root',
})
export class TransactionsService {
  constructor(private commonService: CommonService) {}

  async createTransaction(
    storeId: number | null,
    customerId: number,
    price: number
  ): Promise<any> {
    try {
      return await this.commonService.postRequest('transactions', {
        storeId,
        customerId,
        price,
      });
    } catch (e) {
      throw e;
    }
  }

  async fetchAllTransactions(storeId: number | null): Promise<Transaction[]> {
    return await this.commonService.getRequest(`transactions/store/${storeId}`);
  }

  todayTransactions(transactions: Transaction[]): Transaction[] {
    return transactions.filter((transaction) =>
      this.isToday(new Date(transaction.createdAt))
    );
  }

  isToday = (someDate: any) => {
    const today = new Date();
    return (
      someDate.getDate() === today.getDate() &&
      someDate.getMonth() === today.getMonth() &&
      someDate.getFullYear() === today.getFullYear()
    );
  }
}
