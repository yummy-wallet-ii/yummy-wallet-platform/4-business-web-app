import {Injectable} from '@angular/core';
import {CommonService} from './common.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  connectedUser = null;
  constructor(private commonService: CommonService) {
  }

  getPersonalInfo = async (): Promise<any> => {
    const user = this.connectedUser ? this.connectedUser : await this.commonService.getRequest('users/me');
    return user;
  }
}
